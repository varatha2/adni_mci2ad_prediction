clc;
clear all;

%% aucs 

file_dir = 'MAT_FILES/BL_overfit_testing_v2/';

all_classifier_names = {'MKL-Linear', 'SVM-Linear', 'SVM-RBF', 'GLM-ElasticNet', 'DecisionTree', 'Random Forest'};
% different training sizes for cross-validation
training_data_fracs = 0.8;

% different feature-set sizes
nfeatures_set = [5 : 5 : 90, 94];

all_auc_means = zeros(1, 19, 6);
all_auc_sds = zeros(1, 19, 6);
all_f1_means = zeros(1, 19, 6);
all_sel_metrics = zeros(1, 19, 6);

% loop over different training sizes
for f = 1 : length(training_data_fracs)
    train_frac = training_data_fracs(f);
    
    % loop over different feature lengths
    for l = 1 : length(nfeatures_set)
        nfeatures = nfeatures_set(l);
        
        load(strcat(file_dir, sprintf('cval_struct_tr_%.1f_nf_%d.mat', train_frac, nfeatures)));
        
        field_names = fieldnames(cval_struct);
        
        for k = 1 : length(field_names)
            fn = field_names(k);
            curr_res = eval(strcat('cval_struct.', char(fn)));
            all_auc_means(f, l, k) = mean([curr_res(:).AUC]);
            all_auc_sds(f, l, k) = std([curr_res(:).AUC]);
            all_f1_means(f, l, k) = mean([curr_res(:).F1]);
            all_sel_metrics(f, l, k) = mean([curr_res(:).AUC]) / log(train_frac*100) / log(nfeatures);
        end
        
    end
    
end


%% weights
load('classification_struct.mat');

feature_names = [{'ABeta', 'T-Tau', 'P-Tau', 'ANART', 'AGE', 'APOE_C', 'YOE', 'GENDER', 'MMSE'}, ...
{'APOE', 'BIN1', 'CLU', 'ABCA7', 'CR1', 'PICALM', 'PICALM1', 'MS4A6A', 'CD33', 'CD2AP', 'CD2AP1', 'CD2AP2'}, ...
strcat('Amyloid - ', classification_struct.labels.amyloid_feature_names), ...
classification_struct.labels.fdg_roi_names, ...
classification_struct.labels.mri_roi_names'];

icv_idx = length(feature_names);
sa_idx = find(strncmp(feature_names, 'Surface Area', 12));
excluded_vo_idx = sa_idx - 1;
feature_names([sa_idx, excluded_vo_idx, icv_idx]) = [];

load(strcat(file_dir, 'mkl_weights.mat'));
load(strcat(file_dir, 'svm_linear_weights.mat'));
load(strcat(file_dir, 'glm_lasso_weights.mat'));
load(strcat(file_dir, 'rf_weights.mat'));


f_w_mkl = mkl_weights;
[~, map_mkl] = sort(abs(f_w_mkl), 'descend');
disp('Most predictive features - MKL:');
feature_names(map_mkl(1:30))'

f_w_svm = svm_linear_weights;
[~, map_svm] = sort(abs(f_w_svm), 'descend');
disp('Most predictive features - SVM:');
feature_names(map_svm(1:30))'

f_w_glm = glm_lasso_weights;
[~, map_glm] = sort(abs(f_w_glm), 'descend');
disp('Most predictive features - GLM:');
feature_names(map_glm(1 : sum(f_w_glm ~= 0)))'

f_w_rf = rf_weights;
[~, map_rf] = sort(abs(f_w_rf), 'descend');
disp('Most predictive features - RF:');
feature_names(map_rf(1:30))'

disp('Common predictors:');
common_features = feature_names(intersect(intersect(map_mkl(1:50), map_svm(1:50)), map_glm(1 : sum(f_w_glm ~= 0))))'

%% 
best_classifiers = {};
max_auc_means = zeros(5, 7);
max_auc_ses = zeros(5, 7);

used_classifier_idx = [1 : 6];
for f = 1 : 5
    % loop over different feature lengths
    for l = 1 : 7
        curr_aucs = all_auc_means(f, l, used_classifier_idx);
        [max_auc_means(f, l), max_class] = max(curr_aucs);
        max_auc_ses(f, l) = all_auc_sds(f, l, used_classifier_idx(max_class)) / sqrt(10);
        best_classifiers(f, l) = field_names(used_classifier_idx(max_class));
    end   
end

cl_best_auc_gof_metric_means = zeros(length(used_classifier_idx), 7);
cl_best_auc_gof_metric_ses = zeros(length(used_classifier_idx), 7);
cl_best_auc_tr_sizes = zeros(length(used_classifier_idx), 1);
cl_best_auc_nfeatures = zeros(length(used_classifier_idx), 1);

for l = 1 : length(used_classifier_idx)
    cl = used_classifier_idx(l);
    cl_aucs = squeeze(all_auc_means(:, :, cl));
    [~, idx] = max(cl_aucs(:));
    [m, n] = ind2sub(size(cl_aucs),idx);
    
    train_frac = training_data_fracs(m);
    nfeatures = nfeatures_set(n);
    
    cl_best_auc_tr_sizes(l) = train_frac;
    cl_best_auc_nfeatures(l) = nfeatures;

    load(strcat(file_dir, sprintf('cval_struct_tr_%.1f_nf_%d.mat', train_frac, nfeatures)));
        
    cl_mat = squeeze(cell2mat(struct2cell(eval(strcat('cval_struct.', char(field_names(cl)))))));
    
    cl_best_auc_gof_metric_means(l, :) = mean(cl_mat, 2);
    cl_best_auc_gof_metric_ses(l, :) = std(cl_mat, [], 2) / sqrt(10);
end

%%
used_classifier_idx = [1 2 4];
nfeatures_best = nfeatures_set([1, 13, 5]);
mean_metrics_mat = [];
se_metrics_mat = [];
for l = 1 : length(used_classifier_idx)
    cl = used_classifier_idx(l);
    train_frac = 0.8;
    nfeatures = nfeatures_best(l);
    
    load(strcat(file_dir, sprintf('cval_struct_tr_%.1f_nf_%d.mat', train_frac, nfeatures)));
        
    cl_mat = squeeze(cell2mat(struct2cell(eval(strcat('cval_struct.', char(field_names(cl)))))));
    cl_mat(2:7, :) = cl_mat(2:7, :) / 100;
    mean_metrics_mat = [mean_metrics_mat; mean(cl_mat, 2)'];
    se_metrics_mat = [se_metrics_mat; std(cl_mat, [], 2)' / sqrt(10)];
end

%%

used_classifier_idx = [1 : 6];
max_aucs_per_tr_size = zeros(length(used_classifier_idx), 5);
max_auc_f_len_per_tr_size = zeros(length(used_classifier_idx), 5);
for f = 1 : 5
    curr_aucs = squeeze(all_auc_means(f, :, used_classifier_idx));
    [max_aucs_per_tr_size(:, f), f_len_idx] = max(curr_aucs, [], 1);
    max_auc_f_len_per_tr_size(:, f) = nfeatures_set(f_len_idx);
end

%% plots
x_ticks = (1 ./ (135 * training_data_fracs))' * nfeatures_set;
x_ticks = x_ticks(:);
[~, un_idx] = unique(x_ticks);

used_classifier_idx = [1 2 4];
mean_aucs = [];
se_aucs = [];
for cl = used_classifier_idx
    curr_aucs = all_auc_means(:, :, cl);
    curr_ses = all_auc_sds(:, :, cl) / sqrt(100);
    % curr_aucs = curr_aucs(:);    
    mean_aucs = [mean_aucs curr_aucs(:)];
    se_aucs = [se_aucs curr_ses(:)];
end
[~, sort_idx] = sort(x_ticks);
x_ticks = repmat(x_ticks(:), 1, length(used_classifier_idx));
% mean_mkl_aucs = mean(reshape(permute(all_auc_means(:, :, [1 2 3]), [1 3 2]), 15, 7));
% mean_svm_aucs = mean(reshape(permute(all_auc_means(:, :, 4 : 6), [1 3 2]), 15, 7));
% mean_rf_aucs = mean(all_auc_means(:, :, 14), 1);
% 
% se_mkl_aucs = std(reshape(permute(all_auc_means(:, :, 1:3), [1 3 2]), 15, 7)) / sqrt(15);
% se_svm_aucs = std(reshape(permute(all_auc_means(:, :, 1:3), [1 3 2]), 15, 7)) / sqrt(15);
% se_rf_aucs = std(squeeze(all_auc_means(:, :, 14))) / sqrt(5); 

figure;
errorbar(log(x_ticks(sort_idx, :)), mean_aucs(sort_idx, :), se_aucs(sort_idx, :)); 
% h = plot(log(x_ticks(sort_idx, :)), mean_aucs(sort_idx, :)); 
legend(all_classifier_names(used_classifier_idx));
xt = get(gca, 'XTick');
set(gca, 'XTickLabels', round(exp(xt) * 100) / 100);
ax = gca;
ax.XAxis.TickLabelFormat =  '%,.2f';
ylabel('Cross validated AUC');
xlabel('ratio - #features/#training samples');
% ylim([0.7 0.95]);
linewidth = 2; lineCover = 3*linewidth;
a = [findall(gcf, 'Marker', 'none') findall(gcf, 'Marker', '.')];
set(a, 'LineWidth', linewidth, 'Marker', '.', 'MarkerSize', lineCover);
set(gca, 'fontsize', 20, 'FontWeight', 'bold', 'linewidth', 2);
% hold on
% e = errorbar(log(x_ticks(sort_idx, :)), mean_aucs(sort_idx, :), se_aucs(sort_idx, :)); 
% e(1).Color = h(1).Color;
% e(2).Color = h(2).Color;
% e(3).Color = h(3).Color;
