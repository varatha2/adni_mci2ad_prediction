clc;
clear all;
warning('off','all');

%% pipeline
% This pipeline performs P-MCI to NP-MCI classification

%% load data

load('classification_struct.mat');
% load('control_features.mat');
load('classification_features.mat');
load('classification_labels.mat');

%% dependencies

addpath('GMKL/', 'MIToolbox-3.0.0/matlab/', 'FEAST/matlab/');

feature_names = [{'ABeta', 'T-Tau', 'P-Tau', 'ANART', 'AGE', 'APOE_C', 'YOE', 'GENDER', 'MMSE'}, ...
{'APOE', 'BIN1', 'CLU', 'ABCA7', 'CR1', 'PICALM', 'PICALM1', 'MS4A6A', 'CD33', 'CD2AP', 'CD2AP1', 'CD2AP2'}, ...
strcat('Amyloid - ', classification_struct.labels.amyloid_feature_names), ...
classification_struct.labels.fdg_roi_names, ...
classification_struct.labels.mri_roi_names'];

%% organize data for classification
X = classification_features(classification_labels >= 0, :);
T = classification_labels(classification_labels >= 0);
T(T ~= 1) = -1;

%% feature pre-processing

% divide all the volumes by the total intracranial volume
vo_idx = find(strncmp(feature_names, 'Volume', 6) | strncmp(feature_names, 'Hippocampal', 11));
icv_idx = length(feature_names);

for idx = 1 : length(vo_idx)
    X(:, vo_idx(idx)) = X(:, vo_idx(idx)) ./ X(:, icv_idx);
end

% remove surface areas and Icv from the features
sa_idx = find(strncmp(feature_names, 'Surface Area', 12));
excluded_vo_idx = sa_idx - 1;
X(:, [sa_idx, excluded_vo_idx, icv_idx]) = [];
feature_names([sa_idx, excluded_vo_idx, icv_idx]) = [];

% replace missing values with the median of the corresponding feature
for i = 1 : size(X, 2)
  bad_idx = X(:, i) == -1 | isnan(X(:, i));
  X(bad_idx, i) = median(X(~bad_idx, i));
end

% standardize the features
X_norm = X;

for i = 1 : size(X, 2)
  X_norm(:, i) = (X(:, i) - mean(X(:, i))) / std(X(:, i));
end

%% matrices to keep track of feature weights

mkl_weights = zeros(length(feature_names), 1);
svm_linear_weights = zeros(length(feature_names), 1);
svm_rbf_weights = zeros(length(feature_names), 1);
glm_lasso_weights = zeros(length(feature_names), 1);
rf_weights = zeros(length(feature_names), 1);

%% main loop 

% number of cross-validations 
ncrossval = 5;

% validation on testing or training data
test_on_training = 0;

train_frac = 0.8;
nfeatures = 94;

svm_lin_probs = [];
svm_rbf_probs = [];
glm_lasso_probs = [];
rf_probs = [];
T_all = [];

% 10-fold cross_validation
parfor cval = 1 : ncrossval
    fprintf('Train size - %.1f, #features - %d: cross validation iteration %d..\n', train_frac, nfeatures, cval);

    % get a CV partition for cross validation
    [X_train, X_test, T_train, T_test] = get_CV_partition(X_norm, T, train_frac, 1, test_on_training);
    
    X_test = [X_test; X_train];
    T_test = [T_test; T_train];
    
    T_all = [T_all; T_test];

    tr_odds_ratio = sum(T_train == -1) / sum(T_train == 1);

    %% SVM
    % Standard SVM with linear kernel
    [~, probs, ~] = standard_SVM_with_grid_search(X_train, T_train, X_test, 'linear');
    svm_lin_probs = [svm_lin_probs; probs];

    % Standard SVM with rbf kernel
    [~, probs, ~] = standard_SVM_with_grid_search(X_train, T_train, X_test, 'rbf');
    svm_rbf_probs = [svm_rbf_probs; probs];

%     %% GLM Lasso
%     [~, probs, ~] = glm_lasso_with_grid_search(X_train, T_train, X_test);
%     glm_lasso_probs = [glm_lasso_probs; probs];
% 
%     %% Random forest
%     [~, probs, ~] = RandomForest_with_hyperparameter_opt(X_train, T_train, X_test);
%     rf_probs = [rf_probs; probs];

end

%% plot histogram of projections

figure;
h1 = histfit2(svm_lin_probs(T_all == 1), 40, 'kernel', 'percent');
hold on;
h2 = histfit2(svm_lin_probs(T_all == -1), 40, 'kernel', 'percent');
hold off;

delete(h1(1));
h1(2).Color = 'r';
delete(h2(1));
h2(2).Color = 'g';
legend('P-MCI', 'NP-MCI');
xlabel('Likelihood Probability');
ylabel('Probability');
xlim([0 1]);
title('SVM-Linear');
linewidth = 3; lineCover = 3*linewidth;
a = [findall(gcf, 'Marker', 'none') findall(gcf, 'Marker', '.')];
set(a, 'LineWidth', linewidth, 'Marker', '.', 'MarkerSize', lineCover);
set(gca, 'fontsize', 20, 'FontWeight', 'bold', 'linewidth', 2);

figure;
h1 = histfit2(svm_rbf_probs(T_all == 1), 40, 'kernel', 'percent');
hold on;
h2 = histfit2(svm_rbf_probs(T_all == -1), 40, 'kernel', 'percent');
hold off;

delete(h1(1));
h1(2).Color = 'r';
delete(h2(1));
h2(2).Color = 'g';
legend('P-MCI', 'NP-MCI');
xlabel('Likelihood Probability');
ylabel('Probability');
xlim([0 1]);
title('SVM-RBF');
linewidth = 3; lineCover = 3*linewidth;
a = [findall(gcf, 'Marker', 'none') findall(gcf, 'Marker', '.')];
set(a, 'LineWidth', linewidth, 'Marker', '.', 'MarkerSize', lineCover);
set(gca, 'fontsize', 20, 'FontWeight', 'bold', 'linewidth', 2);

%% scatter
figure; 
gscatter(svm_lin_probs, svm_rbf_probs, T_all,'br', 'xo', 10)
xlabel('SVM-linear probabilities');
ylabel('SVM-RBF probabilities');
legend('NP-MCI', 'P-MCI');
linewidth = 3; lineCover = 3*linewidth;
a = [findall(gcf, 'Marker', 'none') findall(gcf, 'Marker', '.')];
set(a, 'LineWidth', linewidth, 'Marker', '.', 'MarkerSize', lineCover);
set(gca, 'fontsize', 20, 'FontWeight', 'bold', 'linewidth', 2);

% figure;
% h1 = histfit2(glm_lasso_probs(T_all == 1), 40, 'kernel', 'percent');
% hold on;
% h2 = histfit2(glm_lasso_probs(T_all == -1), 40, 'kernel', 'percent');
% hold off;
% 
% delete(h1(1));
% h1(2).Color = 'r';
% delete(h2(1));
% h2(2).Color = 'g';
% legend('P-MCI', 'NP-MCI');
% xlabel('Likelihood Probability');
% ylabel('Probability');
% xlim([0 1]);
% title('GLM-LASSO');
% linewidth = 3; lineCover = 3*linewidth;
% a = [findall(gcf, 'Marker', 'none') findall(gcf, 'Marker', '.')];
% set(a, 'LineWidth', linewidth, 'Marker', '.', 'MarkerSize', lineCover);
% set(gca, 'fontsize', 20, 'FontWeight', 'bold', 'linewidth', 2);
% 
% figure;
% h1 = histfit2(rf_probs(T_all == 1), 40, 'kernel', 'percent');
% hold on;
% h2 = histfit2(rf_probs(T_all == -1), 40, 'kernel', 'percent');
% hold off;
% 
% delete(h1(1));
% h1(2).Color = 'r';
% delete(h2(1));
% h2(2).Color = 'g';
% legend('P-MCI', 'NP-MCI');
% xlabel('Likelihood Probability');
% ylabel('Probability');
% xlim([0 1]);
% title('Random Forest');
% linewidth = 3; lineCover = 3*linewidth;
% a = [findall(gcf, 'Marker', 'none') findall(gcf, 'Marker', '.')];
% set(a, 'LineWidth', linewidth, 'Marker', '.', 'MarkerSize', lineCover);
% set(gca, 'fontsize', 20, 'FontWeight', 'bold', 'linewidth', 2);



% 
% figure;
% histogram(rf_probs(T_all == 1), 50, 'Normalization','probability');
% hold on;
% histogram(rf_probs(T_all == -1), 50, 'Normalization','probability');
% hold off;
% title('Random Forest');
