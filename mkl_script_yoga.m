clc;
clear all;

addpath('GMKL\');

load('classification_struct.mat');
load('control_features.mat');
load('classification_features.mat');
load('classification_labels.mat');

feature_names = [{'ABeta', 'T-Tau', 'P-Tau', 'ANART', 'AGE', 'APOE_C', 'YOE', 'GENDER', 'MMSE'}, ...
{'APOE', 'BIN1', 'CLU', 'ABCA7', 'CR1', 'PICALM', 'PICALM1', 'MS4A6A', 'CD33', 'CD2AP', 'CD2AP1', 'CD2AP2'}, ...
classification_struct.labels.fdg_roi_names, ...
classification_struct.labels.mri_roi_names'];

X = classification_features;
T = double(classification_labels > 0);
T(T == 0) = -1;

% X = classification_features(classification_labels == 1 | classification_labels == 2, :);
% T = classification_labels(classification_labels == 1 | classification_labels == 2);
% T(T == 2) = -1;

for i = 1 : size(X, 2)
  bad_idx = X(:, i) == -1 | isnan(X(:, i));
  X(bad_idx, i) = median(X(~bad_idx, i));
end

X_norm = X;

for i = 1 : size(X, 2)
  X_norm(:, i) = (X(:, i) - mean(X(:, i))) / std(X(:, i));
end

X_all = X_norm';

train_idx = 1 : round(length(T) * 0.5);
test_idx = train_idx(end) + 1 : length(T);

X_train = X_norm(train_idx, :)';
T_train = T(train_idx);

X_test = X_norm(test_idx, :)';
T_test = T(test_idx);


%% mkl
disp('MKL: all-linear');
[labels, probs, svm_struct] = GMKLwrapper_ADNI(X_train, T_train, X_all, T, @kernel_precomp_all_linear);

accuracy=100*mean(labels==T);
Sn = 100*sum(labels==1 & T == 1) / sum(T == 1);
Sp = 100*sum(labels==-1 & T == -1) / sum(T == -1);
[~, ~, ~, AUC] = perfcurve(T, probs, 1);
fprintf('Classification accuracy on the test set = %6.2f%%, Sn = %.4f%%, Sp = %.4f%%, AUC = %.4f\n',accuracy, Sn, Sp, AUC);
f_w = svm_struct.d;
[~, map] = sort(abs(f_w), 'descend');
disp('Most predictive features:');
feature_names(map(1:20))'

disp('MKL: all-rbf');
[labels, probs, svm_struct] = GMKLwrapper_ADNI(X_train, T_train, X_all, T, @kernel_precomp_all_rbf);

accuracy=100*mean(labels==T);
Sn = 100*sum(labels==1 & T == 1) / sum(T == 1);
Sp = 100*sum(labels==-1 & T == -1) / sum(T == -1);
[~, ~, ~, AUC] = perfcurve(T, probs, 1);
fprintf('Classification accuracy on the test set = %6.2f%%, Sn = %.4f%%, Sp = %.4f%%, AUC = %.4f\n',accuracy, Sn, Sp, AUC);
f_w = svm_struct.d;
[~, map] = sort(abs(f_w), 'descend');
disp('Most predictive features:');
feature_names(map(1:20))'

disp('MKL: mixed');
[labels, probs, svm_struct] = GMKLwrapper_ADNI(X_train, T_train, X_all, T, @kernel_precomp);

accuracy=100*mean(labels==T);
Sn = 100*sum(labels==1 & T == 1) / sum(T == 1);
Sp = 100*sum(labels==-1 & T == -1) / sum(T == -1);
[~, ~, ~, AUC] = perfcurve(T, probs, 1);
fprintf('Classification accuracy on the test set = %6.2f%%, Sn = %.4f%%, Sp = %.4f%%, AUC = %.4f\n',accuracy, Sn, Sp, AUC);
f_w = svm_struct.d;
[~, map] = sort(abs(f_w), 'descend');
disp('Most predictive features:');
feature_names(map(1:20))'

% %% pure SVM
% disp('SVM:');
% box_constraint = 1;
% cost_matrix = [ 0.0 0.1; 
%                 1.0 0.0];
%             
% SVMModel = fitcsvm(X_train', T_train, 'BoxConstraint', box_constraint, 'Standardize', 'on', 'Cost', cost_matrix, ...
%     'KernelFunction', 'linear', 'ClassNames', [-1 1]);
% CompactSVMModel = fitPosterior(SVMModel);
% [labels, scores] = predict(CompactSVMModel, X_norm);
% probs = scores(:, 1);
% labels = -1 * labels;
% 
% accuracy=100*mean(labels==T);
% Sn = 100*sum(labels==1 & T == 1) / sum(T == 1);
% Sp = 100*sum(labels==-1 & T == -1) / sum(T == -1);
% [~, ~, ~, AUC] = perfcurve(T, probs, 1);
% fprintf('Classification accuracy on the test set = %6.2f%%, Sn = %.4f%%, Sp = %.4f%%, AUC = %.4f\n',accuracy, Sn, Sp, AUC);
% 
% s = SVMModel.KernelParameters.Scale;
% f_w = SVMModel.Alpha' * SVMModel.SupportVectors;
% [~, map] = sort(abs(f_w), 'descend');
% disp('Most predictive features:');
% feature_names(map(1:20))'