function [X_train, X_test, T_train, T_test] = get_CV_partition(X, T, train_fraction, shuffle, test_on_training)

    % generate a CV partition with specified training data fraction and
    % shuffling. The training data fraction is preserved between the two
    % classes.
    
    X_1 = X(T == 1, :);
    X_0 = X(T == -1, :);
    T_1 = T(T == 1);
    T_0 = T(T == -1);
    
    if(shuffle == 1)
        rand_idx = randperm(size(X_1, 1));
        X_1 = X_1(rand_idx, :);
        T_1 = T_1(rand_idx);
        
        rand_idx = randperm(size(X_0, 1));
        X_0 = X_0(rand_idx, :);
        T_0 = T_0(rand_idx);
    end
    
    tr_idx_1 = 1 : round(length(T_1) * train_fraction);
    ts_idx_1 = tr_idx_1(end) + 1 : length(T_1);
    
    tr_idx_0 = 1 : round(length(T_0) * train_fraction);
    ts_idx_0 = tr_idx_0(end) + 1 : length(T_0);
    
    X_train = [X_1(tr_idx_1, :); X_0(tr_idx_0, :)];
    T_train = [T_1(tr_idx_1); T_0(tr_idx_0)];
    
    if(test_on_training == 1)
        X_test = X_train;
        T_test = T_train;
    else
        X_test = [X_1(ts_idx_1, :); X_0(ts_idx_0, :)];
        T_test = [T_1(ts_idx_1); T_0(ts_idx_0)];
    end
end