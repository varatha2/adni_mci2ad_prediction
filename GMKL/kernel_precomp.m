function D=kernel_precomp(TRNfeatures,TSTfeatures)
  [~,NUMtrain]=size(TRNfeatures);
  [NUMk,NUMtest]=size(TSTfeatures);
  D=zeros(NUMtrain,NUMtest,NUMk);
  load('param_structs.mat');
  for k=1:NUMk 
      if(k <= 3)
          i = 1;
      elseif(k == 4)
          i = 3;
      elseif(k <= 8)
          i = 2;
      elseif(k == 9)
          i = 4;
      elseif(k <= 21)
          i = 5;
      elseif(k <= 27)
          i = 8;
      elseif(k <= 32)
          i = 7;
      elseif(k <= 94)
          i = 6;
      else
          i = 9;
      end
      
      kernel = param_structs(i).params.kernel;
      % box_c = param_structs(i).params.C;
      gamma = param_structs(i).params.gamma;
      
      if(strcmp(kernel, 'linear'))
          D(:,:,k)= repmat(TRNfeatures(k,:)',1,NUMtest).*repmat(TSTfeatures(k,:),NUMtrain,1);
      elseif(strcmp(kernel, 'rbf'))
          D(:,:,k)=exp(-gamma*(repmat(TRNfeatures(k,:)',1,NUMtest)-repmat(TSTfeatures(k,:),NUMtrain,1)).^2);
      end
  end
%     modalities = {'CSF', 'DEMOGRAPHIC', 'ANART', 'CLINICAL', 'DNA', 'MRI', 'PET'};
%     load('param_structs.mat');
%     for i = 1 : 7
%         modality = modalities(i);
%         % disp(modality);
% 
%         if(strcmp(modality, 'CSF'))
%             feature_idx = 1 : 3;
%         elseif(strcmp(modality, 'DEMOGRAPHIC'))
%             feature_idx = 5 : 8;
%         elseif(strcmp(modality, 'ANART'))
%             feature_idx = 4;
%         elseif(strcmp(modality, 'CLINICAL'))
%             feature_idx = 9;
%         elseif(strcmp(modality, 'DNA'))
%             feature_idx = 10 : 21;
%         elseif(strcmp(modality, 'MRI'))
%             feature_idx = 27 : 159;
%         elseif(strcmp(modality, 'PET'))
%             feature_idx = 22 : 26;
%         end
% 
%         kernel = param_structs(i).params.kernel;
%         box_c = param_structs(i).params.C;
%         gamma = param_structs(i).params.gamma;
%         
%         for k = feature_idx
%             if(strcmp(kernel, 'linear'))
%                 D(:,:,k)=repmat(TRNfeatures(k,:)',1,NUMtest).*repmat(TSTfeatures(k,:),NUMtrain,1);
%             elseif(strcmp(kernel, 'rbf'))
%                 D(:,:,k)=exp(-(repmat(TRNfeatures(k,:)',1,NUMtest)-repmat(TSTfeatures(k,:),NUMtrain,1)).^2);
%             end
%         end
%     end

end