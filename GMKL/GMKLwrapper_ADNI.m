function [pred_labels, pred_probs, svm_struct] = GMKLwrapper_ADNI(X_train, T_train, X_test, T_test, precomp)
% Generalized Multiple Kernel Learning wrapper function.
%
% This function shows how to call COMPGDoptimize. The code is general
% and you can plug in your own kernel function and regularizer. To
% illustrate, I've included code for computing sums and products of
% RBF kernels across features based on both feature vectors and
% precomputed kernel matrices.
%
% MATLAB's quadprog will only work for small data sets. Also, I've
% only provide code for C-SVC. Note that you must have the
% optimization toolbox installed before you can use quadprog.
%
% If you would like to use LIBSVM (for SVR/SVC) instead, please 
% set parms.TYPEsolver='LIB'. In this case, you must have the 
% appropriate (modified) svmtrain.mexxxx in your path. Compiled 32 and
% 64 bit binaries for Windows and Linux are provided in
% ../LIBSVM/Binaries but you'll need to check whether they're compatible
% with your version of MATLAB. The modified source code is provided in
% ../LIBSVM/Code/libsvm-2.84-1 and you can compile that directly if you
% prefer. 
%
% You can also incorporate your favourite SVM solver by editing
% COMPGDoptimize.m.
%
% Code written by Manik Varma.

%   addpath('../');
%   load('classification_struct.mat');
%   load('control_features.mat');
%   load('classification_features.mat');
%   load('classification_labels.mat');
%   
%   feature_names = [{'ABeta', 'T-Tau', 'P-Tau', 'ANART', 'AGE', 'APOE_C', 'YOE', 'GENDER', 'MMSE'}, ...
%     {'APOE', 'BIN1', 'CLU', 'ABCA7', 'CR1', 'PICALM', 'PICALM1', 'MS4A6A', 'CD33', 'CD2AP', 'CD2AP1', 'CD2AP2'}, ...
%     classification_struct.labels.fdg_roi_names, ...
%     classification_struct.labels.mri_roi_names'];
% 
%   X = classification_features;
%   T = double(classification_labels > 0);
%   T(T == 0) = -1;
%   
%   for i = 1 : size(X, 2)
%       bad_idx = X(:, i) == -1 | isnan(X(:, i));
%       X(bad_idx, i) = median(X(~bad_idx, i));
%   end
%   
%   X_norm = X;
% 
%   for i = 1 : size(X, 2)
%       X_norm(:, i) = (X(:, i) - mean(X(:, i))) / std(X(:, i));
%   end
% 
%   
%   train_idx = 1 : round(length(T) / 2);
%   test_idx = train_idx(end) + 1 : length(T);
% 
%   X_train = X_norm(train_idx, :)';
%   T_train = T(train_idx);
% 
%   X_test = X_norm(test_idx, :)';
%   T_test = T(test_idx);

  TYPEreg=0;
  NUMkernels=size(X_train, 1);
  
  %% sum of RBF kernels
%   % The following code shows how to call COMPGDoptimize with
%   % feature vectors as input
%   TYPEker=0;
%   parms=initparms(NUMkernels,TYPEker,TYPEreg);
%   svm=COMPGDoptimize(X_train,T_train,parms);
%   feature_weights = svm.d;
%   
%   % The following code shows how to use the learnt svm for classifying
%   % test data.
%   K=parms.fncK(X_train(:,svm.svind),X_test,parms,svm.d);
  
%   %% product of exp of precomputed
%   % The following code shows how to call COMPGDoptimize with
%   % precomputed matrices as input
%   global Dprecomp;
%   
%   TYPEker=3;
%   parms=initparms(NUMkernels,TYPEker,TYPEreg);
%   Dprecomp=precomp(X_train,X_train);
%   % fprintf('Learning products of RBF kernels subject to l1 regularization [using pre-computed distance matrices].\n');
%   svm=COMPGDoptimize([1:size(X_train,2)],T_train,parms);
%   % fprintf('At this point, svm.d shold be [1.020 0.353]''\n');
%   feature_weights = svm.d;
% 
%   % The following code shows how to use the learnt svm for classifying
%   % test data.
%   Dprecomp=precomp(X_train,X_test);
%   K=parms.fncK(svm.svind,[1:size(X_test,2)],parms,svm.d);
  
  %% sum of precomputed
  % The following code shows how to call COMPGDoptimize with
  % precomputed matrices as input
  global Kprecomp;
  
  TYPEker=2;
  parms=initparms(NUMkernels,TYPEker,TYPEreg);
  Kprecomp=precomp(X_train,X_train);
  % fprintf('Learning products of RBF kernels subject to l1 regularization [using pre-computed distance matrices].\n');
  svm_struct=COMPGDoptimize([1:size(X_train,2)],T_train,parms);

  % The following code shows how to use the learnt svm for classifying
  % test data.
  Kprecomp=precomp(X_train,X_test);
  K=parms.fncK(svm_struct.svind,[1:size(X_test,2)],parms,svm_struct.d);
  
  
  pred_labels=sign(svm_struct.b+svm_struct.alphay'*K)';
  pred_probs = 1 ./ (1 + exp(-(svm_struct.b+svm_struct.alphay'*K)));
    
end

% function D=precomp(TRNfeatures,TSTfeatures)
%   [NUMk,NUMtrain]=size(TRNfeatures);
%   [NUMk,NUMtest]=size(TSTfeatures);
%   D=zeros(NUMtrain,NUMtest,NUMk);
%   for k=1:NUMk, D(:,:,k)=(repmat(TRNfeatures(k,:)',1,NUMtest)-repmat(TSTfeatures(k,:),NUMtrain,1)).^2; end;
% end

function parms=initparms(NUMk,TYPEker,TYPEreg)
  % Kernel parameters
  switch (TYPEker),
    case 0, parms.KERname='Sum of RBF kernels across features';
            parms.gamma=ones(NUMk,1);
	    parms.fncK=@KSumRBF;
	    parms.fncKdash=@KdashSumRBF;
	    parms.fncKdashint=@KdashSumRBFintermediate;
    case 1, parms.KERname='Product of RBF kernels across features';
            parms.gamma=1;
	    parms.fncK=@KProdRBF;
	    parms.fncKdash=@KdashProdRBF;
	    parms.fncKdashint=@KdashProdRBFintermediate;
    case 2, parms.KERname='Sum of precomputed kernels';
	    parms.fncK=@KSumPrecomp;
	    parms.fncKdash=@KdashSumPrecomp;
	    parms.fncKdashint=@KdashSumPrecompintermediate;
    case 3, parms.KERname='Product of exponential kernels of precomputed disance matrices';
            parms.gamma=1;
	    parms.fncK=@KProdExpPrecomp;
	    parms.fncKdash=@KdashProdExpPrecomp;
	    parms.fncKdashint=@KdashProdExpPrecompintermediate;
    otherwise, fprintf('Unknown kernel type.\n'); keyboard; 
  end;
      
  % Regularization parameters
  switch (TYPEreg),
    case 0, parms.REGname='l1';          % L1 Regularization
            parms.sigma=ones(NUMk,1);
	    parms.fncR=@Rl1;
	    parms.fncRdash=@Rdashl1;
    case 1, parms.REGname='l2';          % L2 Regularization
            parms.mud=ones(NUMk,1);
	    parms.covd=1e-1*eye(NUMk);
	    parms.invcovd=inv(parms.covd);
	    parms.fncR=@Rl2;
	    parms.fncRdash=@Rdashl2;
    otherwise, fprintf('Unknown regularisation type.\n'); keyboard; 
  end;
    
  % Standard SVM parameters
  parms.TYPEprob=0;           % 0 = C-SVC, 3 = EPS-SVR (try others at your own risk)
  parms.C=10;                 % Misclassification penalty for SVC/SVR ('-c' in libSVM)
  
  % Gradient descent parameters
  parms.initd=rand(NUMk,1);   % Starting point for gradient descent
  parms.TYPEsolver='MAT';     % Use Matlab's quadprog as an SVM solver.
  parms.TYPEstep=1;           % 0 = Armijo, 1 = Variant Armijo, 2 = Hessian (not yet implemented)
  parms.MAXITER=50;           % Maximum number of gradient descent iterations
  parms.MAXEVAL=200;          % Maximum number of SVM evaluations
  parms.MAXSUBEVAL=20;        % Maximum number of SVM evaluations in any line search
  parms.SIGMATOL=0.3;         % Needed by Armijo, variant Armijo for line search
  parms.BETAUP=2.1;           % Needed by Armijo, variant Armijo for line search
  parms.BETADN=0.3;           % Needed by variant Armijo for line search
  parms.BOOLverbose=false;     % Print debug information at each iteration
  parms.SQRBETAUP=parms.BETAUP*parms.BETAUP;    
end

function r=Rl1(parms,d), r=parms.sigma'*d; end
function rdash=Rdashl1(parms,d), rdash=parms.sigma; end

function r=Rl2(parms,d), delta=parms.mud-d;r=0.5*delta'*parms.invcovd*delta; end
function rdash=Rdashl2(parms,d), rdash=parms.invcovd*(d-parms.mud); end

function K=KSumRBF(TRNfeatures,TSTfeatures,parms,d)
  [NUMdims,NUMtrn]=size(TRNfeatures);
  [NUMdims,NUMtst]=size(TSTfeatures);
  if (NUMdims~=length(d)), fprintf('NUMdims not equal to NUMk\n'); keyboard; end;
  
  nzind=find(d>1e-4);
  K=zeros(NUMtrn,NUMtst);
  if (~isempty(nzind)),
    for i=1:length(nzind),
      k=nzind(i);
      Dk=(repmat(TRNfeatures(k,:)',1,NUMtst)-repmat(TSTfeatures(k,:),NUMtrn,1)).^2;
      K=K+d(k)*exp(-parms.gamma(k)*Dk);
    end;
  end;
end

function Kdashint=KdashSumRBFintermediate(TRNfeatures,TSTfeatures,parms,d)
  Kdashint=[];
end

function Kdash=KdashSumRBF(TRNfeatures,TSTfeatures,parms,d,k,Kdashint)
  [NUMdims,NUMtrn]=size(TRNfeatures);
  [NUMdims,NUMtst]=size(TSTfeatures);

  Kdash=exp(-parms.gamma(k)*(repmat(TRNfeatures(k,:)',1,NUMtst)-repmat(TSTfeatures(k,:),NUMtrn,1)).^2);
end

function K=KProdRBF(TRNfeatures,TSTfeatures,parms,d)
  [NUMdims,NUMtrn]=size(TRNfeatures);
  [NUMdims,NUMtst]=size(TSTfeatures);
  if (NUMdims~=length(d)), fprintf('NUMdims not equal to NUMk\n'); keyboard; end;
  
  nzind=find(d>1e-4);
  K=zeros(NUMtrn,NUMtst);
  if (~isempty(nzind)),
    for i=1:length(nzind),
      k=nzind(i);
      K=K+d(k)*(repmat(TRNfeatures(k,:)',1,NUMtst)-repmat(TSTfeatures(k,:),NUMtrn,1)).^2;
    end;
  end;
  K=exp(-parms.gamma*K);
end

function Kdashint=KdashProdRBFintermediate(TRNfeatures,TSTfeatures,parms,d)
  Kdashint=-parms.gamma*parms.fncK(TRNfeatures,TSTfeatures,parms,d);
end

function Kdash=KdashProdRBF(TRNfeatures,TSTfeatures,parms,d,k,Kdashint)
  [NUMdims,NUMtrn]=size(TRNfeatures);
  [NUMdims,NUMtst]=size(TSTfeatures);

  Kdash=Kdashint.*(repmat(TRNfeatures(k,:)',1,NUMtst)-repmat(TSTfeatures(k,:),NUMtrn,1)).^2;
end

function K=KSumPrecomp(TRNfeatures,TSTfeatures,parms,d)
  global Kprecomp;
  if (size(Kprecomp,3)~=length(d)), fprintf('Number of kernels does not equal number of weights.\n');keyboard; end;

  K=lincomb(Kprecomp(TRNfeatures,TSTfeatures,:),d);
end

function Kdashint=KdashSumPrecompintermediate(TRNfeatures,TSTfeatures,parms,d)
  Kdashint=[];
end

function Kdash=KdashSumPrecomp(TRNfeatures,TSTfeatures,parms,d,k,Kdashint)
  global Kprecomp;
  
  Kdash=Kprecomp(TRNfeatures,TSTfeatures,k);
end

function K=KProdExpPrecomp(TRNfeatures,TSTfeatures,parms,d)
  global Dprecomp;
  if (size(Dprecomp,3)~=length(d)), fprintf('Number of kernels does not equal number of weights.\n');keyboard; end;

  K=exp(-parms.gamma*lincomb(Dprecomp(TRNfeatures,TSTfeatures,:),d));
end

function Kdashint=KdashProdExpPrecompintermediate(TRNfeatures,TSTfeatures,parms,d)
  Kdashint=-parms.gamma*parms.fncK(TRNfeatures,TSTfeatures,parms,d);
end

function Kdash=KdashProdExpPrecomp(TRNfeatures,TSTfeatures,parms,d,k,Kdashint)
  global Dprecomp;
  
  Kdash=Kdashint.*Dprecomp(TRNfeatures,TSTfeatures,k);
end

function K=lincomb(base,d)
  nzind=find(d>1e-4);
  K=zeros(size(base,1),size(base,2));
  if (~isempty(nzind)), for k=1:length(nzind), K=K+d(nzind(k))*base(:,:,nzind(k)); end; end;
end
