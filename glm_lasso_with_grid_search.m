
function [pred_labels, probs, feature_weights] = glm_lasso_with_grid_search(X_train, T_train, X_test)
    % selects best alpha and lambda. 10 fold crossvalidated deviance is used to select the hyper-parameters
    
    n_lambda = 25;
    alpha_range = 0.1 : 0.1 : 1;
    
    best_alpha = 1;
    best_lambda = 0.35;
    min_dev = 1000;
    
    % convert labels to {0, 1}
    T_train(T_train == -1) = 0;
    
    for alpha = alpha_range
        [~, FitInfo] = lassoglm(X_train, T_train, 'binomial', 'CV', 10, 'NumLambda', n_lambda, 'Alpha', alpha);
        if(FitInfo.Deviance(FitInfo.Index1SE) < min_dev)
            best_alpha = alpha;
            best_lambda = FitInfo.Lambda(FitInfo.Index1SE);
        end
    end

    [B, FitInfo] = lassoglm(X_train, T_train, 'binomial', 'Lambda', best_lambda, 'Alpha', best_alpha);
    B1 = [FitInfo.Intercept;B];
    
    probs = glmval(B1, X_test, 'logit');
    pred_labels = probs > 0.5;
    pred_labels(pred_labels == 0) = -1;
    feature_weights = B;    
end