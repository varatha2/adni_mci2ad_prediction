function [pred_labels, probs, tree_struct] = classify_DecisionTree(X_train, T_train, X_test)
    %% Dtree
    tree_struct = fitctree(X_train,T_train);
    [pred_labels, scores] = predict(tree_struct, X_test);
    probs = scores(:, 2);
end