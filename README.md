# adni_mci2ad_prediction

Matlab code for performing machine learning classification of MCI to AD progressors Vs. non-progressors using multiple biomarkers.

This library was tested on Windows 7 and MacOS High Sierra operating systems.

RUN:
MCIP_workflow_2.m

De-identified features and labels used for classification are provided in mat files.
These mat files are used to run the scripts.