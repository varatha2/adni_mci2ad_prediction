clc;
clear all;
warning('off','all');

%% pipeline
% This pipeline performs P-MCI to NP-MCI classification

%% load data

load('classification_struct.mat');
% load('control_features.mat');
load('classification_features.mat');
load('classification_labels.mat');

%% dependencies

addpath('GMKL/', 'MIToolbox-3.0.0/matlab/', 'FEAST/matlab/');

feature_names = [{'ABeta', 'T-Tau', 'P-Tau', 'ANART', 'AGE', 'APOE_C', 'YOE', 'GENDER', 'MMSE'}, ...
{'APOE', 'BIN1', 'CLU', 'ABCA7', 'CR1', 'PICALM', 'PICALM1', 'MS4A6A', 'CD33', 'CD2AP', 'CD2AP1', 'CD2AP2'}, ...
strcat('Amyloid - ', classification_struct.labels.amyloid_feature_names), ...
classification_struct.labels.fdg_roi_names, ...
classification_struct.labels.mri_roi_names'];

%% organize data for classification
X = classification_features(classification_labels >= 0, :);
T = classification_labels(classification_labels >= 0);
T(T ~= 1) = -1;

%% feature pre-processing

% divide all the volumes by the total intracranial volume
vo_idx = find(strncmp(feature_names, 'Volume', 6) | strncmp(feature_names, 'Hippocampal', 11));
icv_idx = length(feature_names);

for idx = 1 : length(vo_idx)
    X(:, vo_idx(idx)) = X(:, vo_idx(idx)) ./ X(:, icv_idx);
end

% remove surface areas and Icv from the features
sa_idx = find(strncmp(feature_names, 'Surface Area', 12));
excluded_vo_idx = sa_idx - 1;
X(:, [sa_idx, excluded_vo_idx, icv_idx]) = [];
feature_names([sa_idx, excluded_vo_idx, icv_idx]) = [];

% replace missing values with the median of the corresponding feature
for i = 1 : size(X, 2)
  bad_idx = X(:, i) == -1 | isnan(X(:, i));
  X(bad_idx, i) = median(X(~bad_idx, i));
end

% standardize the features
X_norm = X;

for i = 1 : size(X, 2)
  X_norm(:, i) = (X(:, i) - mean(X(:, i))) / std(X(:, i));
end

%% matrices to keep track of feature weights

mkl_weights = zeros(length(feature_names), 1);
svm_linear_weights = zeros(length(feature_names), 1);
svm_rbf_weights = zeros(length(feature_names), 1);
glm_lasso_weights = zeros(length(feature_names), 1);
rf_weights = zeros(length(feature_names), 1);

%% main loop 

% different training sizes for cross-validation
training_data_fracs = 0.5 : 0.1 : 0.9;

% different feature-set sizes
nfeatures_set = [5 10 20 40 60 80 94];

% number of cross-validations 
ncrossval = 10;

% validation on testing or training data
test_on_training = 1;

% loop over different training sizes
for train_frac = training_data_fracs
    
    % get a CV partition for feature selection
    [X_train_f, ~, T_train_f, ~] = get_CV_partition(X_norm, T, train_frac, 0, 0);
    
    % loop over different feature lengths
    for l = 1 : length(nfeatures_set)
        nfeatures = nfeatures_set(l);
        
        mkl_weights_curr = zeros(nfeatures, ncrossval);
        svm_linear_weights_curr = zeros(nfeatures, ncrossval);
        svm_rbf_weights_curr = zeros(nfeatures, ncrossval);
        glm_lasso_weights_curr = zeros(nfeatures, ncrossval);
        rf_weights_curr = zeros(nfeatures, ncrossval);
        
        sel_feature_idx = feast('jmi', nfeatures, X_train_f, T_train_f);

        % 10-fold cross_validation
        parfor cval = 1 : ncrossval
            fprintf('Train size - %.1f, #features - %d: cross validation iteration %d..\n', train_frac, nfeatures, cval);

            % get a CV partition for cross validation
            [X_train, X_test, T_train, T_test] = get_CV_partition(X_norm(:, sel_feature_idx), T, train_frac, 1, test_on_training)
            
            tr_odds_ratio = sum(T_train == -1) / sum(T_train == 1);

            %% MKL
            % MKL - with all linear kernels
            [pred_labels, probs, mkl_model] = GMKLwrapper_ADNI(X_train', T_train, X_test', T_test, @kernel_precomp_all_linear);
            cval_struct_mkl_linear(cval) = goodness_of_fit_metrics(T_test, pred_labels, probs, tr_odds_ratio);

            %% SVM
            % Standard SVM with linear kernel
            [pred_labels, probs, svm_model_lin] = standard_SVM_with_grid_search(X_train, T_train, X_test, 'linear');
            cval_struct_svm_linear(cval) = goodness_of_fit_metrics(T_test, pred_labels, probs, tr_odds_ratio);

            % Standard SVM with rbf kernel
            [pred_labels, probs, svm_model_rbf] = standard_SVM_with_grid_search(X_train, T_train, X_test, 'rbf');
            cval_struct_svm_rbf(cval) = goodness_of_fit_metrics(T_test, pred_labels, probs, tr_odds_ratio);
            
            %% GLM Lasso
            [pred_labels, probs, feature_weights_glm] = glm_lasso_with_grid_search(X_train, T_train, X_test);
            cval_struct_glm_lasso(cval) = goodness_of_fit_metrics(T_test, pred_labels, probs, tr_odds_ratio);

            %% Decision tree
            % standard Decision tree
            [pred_labels, probs, ~] = classify_DecisionTree(X_train, T_train, X_test);
            cval_struct_dtree(cval) = goodness_of_fit_metrics(T_test, pred_labels, probs, tr_odds_ratio);

            % Random forest
            [pred_labels, probs, rf_model] = RandomForest_with_hyperparameter_opt(X_train, T_train, X_test);
            cval_struct_rf(cval) = goodness_of_fit_metrics(T_test, pred_labels, probs, tr_odds_ratio);    

        
            %% feature importance
            mkl_weights_curr(:, cval) = abs(mkl_model.d .* (mkl_model.alphay' * X_train(mkl_model.svind, :))');
            svm_linear_weights_curr(:, cval) = abs(svm_model_lin.Alpha' * svm_model_lin.SupportVectors);
            glm_lasso_weights_curr(:, cval) = abs(feature_weights_glm);
            rf_weights_curr(:, cval) = abs(rf_model.OOBPermutedVarDeltaError);

        end
        mkl_weights(sel_feature_idx) = sum(mkl_weights_curr, 2);
        svm_linear_weights(sel_feature_idx) = sum(svm_linear_weights_curr, 2);
        glm_lasso_weights(sel_feature_idx) = sum(glm_lasso_weights_curr, 2);
        rf_weights(sel_feature_idx) = sum(rf_weights_curr, 2);
        
        cval_struct.mkl_linear = cval_struct_mkl_linear;
        
        cval_struct.svm_linear = cval_struct_svm_linear;
        cval_struct.svm_rbf = cval_struct_svm_rbf;
        
        cval_struct.glm_lasso = cval_struct_glm_lasso;
                
        cval_struct.dtree = cval_struct_dtree;
        cval_struct.rf = cval_struct_rf;
        
        save(sprintf('MATFILES/BL/cval_struct_tr_%.1f_nf_%d.mat', train_frac, nfeatures), 'cval_struct', '-v7.3');
    end
end

save('MATFILES/BL/mkl_weights.mat', 'mkl_weights', '-v7.3');
save('MATFILES/BL/svm_linear_weights.mat', 'svm_linear_weights', '-v7.3');
save('MATFILES/BL/glm_lasso_weights.mat', 'glm_lasso_weights', '-v7.3');
save('MATFILES/BL/rf_weights.mat', 'rf_weights', '-v7.3');
