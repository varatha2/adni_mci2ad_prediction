
function [pred_labels, probs, svm_struct] = standard_SVM_with_grid_search(X_train, T_train, X_test, kernel)
    % selects box-constraint and gamma parameters for svm classification based on data
    % for a specified kernel. 10 fold crossvalidated AUC is used to select the hyper-parameters
    
    % To determine good values of the remaining free parameters (eg, by CV), 
    % it is important to search on the right scale. We therefore fix default 
    % values for C and ? that have the right order of magnitude. 
    % In a c-class problem we use the 1/c quantile of the pairwise distances 
    % D?ij of all data-points as a default for ?. The default for C is the 
    % inverses of the empirical variance s2 in features space, which can be 
    % calculated by s2=1n?iKii?1n2?i,jKij from a n�n kernel matrix K.
    
    maxAUC = 0;
    
    if(strcmp(kernel, 'linear'))
        D = pdist(X_train);
        D = squareform(D);
        sigma_vals = sqrt(2) * [0.25 0.5 1 2 4] * median(D(:)); % kernel = X_i*X_j'/sigma^2

        for sigma = 1 : length(sigma_vals)
            gamma = 1 / (sigma_vals(sigma)^2);
            K = gamma * (X_train * X_train');
            s2 = trace(K) / size(K, 1) - sum(sum(K)) / (size(K, 1)^2);
            box_c_vals = [0.01 0.1 1 10 100] / s2;

            for box_c = 1 : length(box_c_vals)
                SVMModel = fitcsvm(X_train, T_train, 'BoxConstraint', box_c_vals(box_c), 'KernelFunction', char(kernel),...
                    'KernelScale', sigma_vals(sigma), 'ClassNames', [-1 1]);
                CVSVMModel = crossval(SVMModel);
                [~,scores,~] = kfoldPredict(CVSVMModel);
                [~, ~, ~, kAUC] = perfcurve(T_train, scores(:, 2), 1);
                if(kAUC < 0.5); kAUC = 1 - kAUC; end
                if(kAUC > maxAUC)
                    maxAUC = kAUC;
                    CompactSVMModel = fitPosterior(SVMModel);
                end
            end
        end
    elseif(strcmp(kernel, 'rbf'))
        D = pdist(X_train);
        D = squareform(D);
        sigma_vals = sqrt(2) * [0.25 0.5 1 2 4] * median(D(:)); % kernel = exp(-1/sigma^2(X_i - X_j)^2)

        for sigma = 1 : length(sigma_vals)
            gamma = 1 / (sigma_vals(sigma)^2);
            K = exp(-gamma*D);
            s2 = trace(K) / size(K, 1) - sum(sum(K)) / (size(K, 1)^2);
            box_c_vals = [0.01 0.1 1 10 100] / s2;

            for box_c = 1 : length(box_c_vals)
                SVMModel = fitcsvm(X_train, T_train, 'BoxConstraint', box_c_vals(box_c), 'KernelFunction', char(kernel),...
                    'KernelScale', sigma_vals(sigma), 'ClassNames', [-1 1]);
                CVSVMModel = crossval(SVMModel);
                [~,scores,~] = kfoldPredict(CVSVMModel);
                [~, ~, ~, kAUC] = perfcurve(T_train, scores(:, 2), 1);
                if(kAUC < 0.5); kAUC = 1 - kAUC; end
                if(kAUC > maxAUC)
                    maxAUC = kAUC;
                    CompactSVMModel = fitPosterior(SVMModel);
                end
            end
        end
    elseif(strcmp(kernel, 'mixed'))
        K = kernel_precomp_equal_weight(X_train, X_train);
        s2 = trace(K) / size(K, 1) - sum(sum(K)) / (size(K, 1)^2);
        box_c_vals = [0.01 0.1 1 10 100] / s2;

        for box_c = 1 : length(box_c_vals)
            SVMModel = fitcsvm(X_train, T_train, 'BoxConstraint', box_c_vals(box_c), 'KernelFunction', ...
                'kernel_precomp_equal_weight', 'ClassNames', [-1 1]);
            CVSVMModel = crossval(SVMModel);
            [~,scores,~] = kfoldPredict(CVSVMModel);
            [~, ~, ~, kAUC] = perfcurve(T_train, scores(:, 2), 1);
            if(kAUC < 0.5); kAUC = 1 - kAUC; end
            if(kAUC > maxAUC)
                maxAUC = kAUC;
                CompactSVMModel = fitPosterior(SVMModel);                
            end
        end
    end
    [pred_labels, scores] = predict(CompactSVMModel, X_test);
    if(size(scores, 2) == 2)
        probs = scores(:, 2);
    else
        probs = 1 - scores(:, 1);
    end
    svm_struct = CompactSVMModel;
    
end