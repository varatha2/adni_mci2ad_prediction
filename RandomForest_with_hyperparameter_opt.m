function [pred_labels, probs, rf_struct] = RandomForest_with_hyperparameter_opt(X_train, T_train, X_test)
    %% RF
    nTrees = 50;
    fboot = 0.3;

    % Train the TreeBagger (Decision Forest).
    B = TreeBagger(nTrees, X_train, T_train, 'FBoot', fboot, 'SampleWithReplacement', 'on', 'Method','Classification','OOBVarImp','On');

    % Use the trained Decision Forest.
    [pred_labels, scores] = B.predict(X_test);
    pred_labels = str2double(pred_labels);
    if(size(scores, 2) == 2)
        probs = scores(:, 2);
    else
        probs = 1 - scores(:, 1);
    end
    rf_struct = B;
end